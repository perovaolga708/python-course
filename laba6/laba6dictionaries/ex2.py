sentence="To explore different supervised learning algorithms, we're going to use a combination of small synthetic or artificial datasets as examples, " \
         "together with some larger real world datasets."
lis= sentence.replace(',', '').split()#split the line

dictionary = {lis:len(lis) for lis in (lis)}#the  key is lis element,the value is lengthof lis for each element of lis in lis
print(dictionary)
#output:{'To': 2, 'explore': 7, 'different': 9, 'supervised': 10, 'learning': 8, \
# 'algorithms': 10, "we're": 5, 'going': 5, 'to': 2, 'use': 3, 'a': 1, 'combination': 11, 'of': 2, 'small': 5, \
# 'synthetic': 9, 'or': 2, 'artificial': 10, 'datasets': 8, 'as': 2, 'examples': 8, 'together': 8, \
# 'with': 4, 'some': 4, 'larger': 6,\
# 'real': 4, 'world': 5, 'datasets.': 9}


